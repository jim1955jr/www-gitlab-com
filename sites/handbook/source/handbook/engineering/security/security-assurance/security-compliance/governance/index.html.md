---
layout: handbook-page-toc
title: "Security Governance Program"
description: "Security Governance Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
The 'G' in [GRC](https://www.oceg.org/about/what-is-grc/), GitLab's security governance discipline helps to define, train and measure security strategies and progress toward security objectives by creating a set of processes and practices that run across departments and functions.  By following a Governance framework, GitLab ensures accountability, fairness and transparency in both how the company runs and how it communicates with its stakeholders. 

## Core Competencies
These are the core responsibilities of the security governance discipline.

### Security Compliance Handbook Pages: Policies and Standards
Keeping the organization on track and within established boundaries to ensure compliance with laws and regulations. Providing guidance, consistency and accountability to streamline internal processes and align with GitLab's values and mission.
[Insert: Information Security Policy]

### Security Compliance Metrics
Measuring performance effectiveness of our security controls, against a plan to prevent security incidents and safeguard sensitive data to improve the security posture of GitLab and the reduction of risk.  "If you cannot measure it, you cannot improve it" -Lord Kevin.

### Regulatory and Compliance Landscape Monitoring
As the world of regulatory compliance is always evolving and GitLab is growing, it is important to continue monitoring for changes, updating existing controls and implementing new regulations as needed helps to improve the security of GitLab.

### GCF Control Maintenance
Managing the GCF control framework, to include changes as a result of the risks and regulatory requirements. 

### Security Compliance Training
Creating and managing security compliance trainings to ensure GitLab team members are aware and trained in security core competencies.

### GRC Application Administration
Managing all compliance activities within ZenGRC such as Control Testing, UARs, Vendor Reviews and Risk Assessments to automate, integrate and streamline business processes to increase GitLab's Information Security Program maturity and deliver measurable ROI. 
